# Firehydrants.com
## Observations
* This design doesn't considerate a collaborative work (e.g., Two doggies could test in half of the time).

## Requirements
* Java8

## Running the server
* Use: *./mvnw spring-boot:run*
* Add more doggies: Go to the class *FirehydrantsApplication* and add any doggies to the list.

## Running Tests
* Use: *./mvnw test*

### Functions
* http://localhost:8080/canSellHydrant
* http://localhost:8080/sellHydrant
* http://localhost:8080/doggies - List of all doggies (Only for Testing)
* http://localhost:8080/first - First doggy available to test (Only for Testing)
* http://localhost:8080/h2  - Database interface (Only for Testing)

## Cases
### 5 Minutes Periodo
The test for this case is *com.firehydrants.rest.controllers.FirstPartTest*
### 5 Minutes Periodo + 5 test in an hour
The test for this case is *com.firehydrants.rest.controllers.SecondPartTest*
### Three doggies and 5 Minutes Periodo + 5 test in an hour
The test for this case is *com.firehydrants.rest.controllers.ThirdPartTest*
### 10 test in an hour
There is not test for this case but it will be easy update the value *MAX_TOTAL_TEST* to 10 in the class *com.firehydrants.rules.OneHourPeriod*
