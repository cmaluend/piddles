package com.firehydrants.rules;

import java.time.LocalDateTime;

public class FiveMinutesPeriod extends RuleBase {

    private static final int WAIT_MINUTES = 5;

    @Override
    public LocalDateTime startShift() {
        super.setNextShift(LocalDateTime.now().plusMinutes(WAIT_MINUTES));
        return super.getNextShift();
    }

    @Override
    public String getJsonDump() {
        return "{}";
    }

    /**
     * Only for testing.
     * @return
     */
    public static int getWaitMinutes() {
        return WAIT_MINUTES;
    }

}
