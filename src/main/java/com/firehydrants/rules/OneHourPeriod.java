package com.firehydrants.rules;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OneHourPeriod extends RuleBase {

    private static final int WAIT_MINUTES = 60;

    /**
     * Extra Credit.
     * Change this value to 10;
     */
    private static final int MAX_TOTAL_TEST = 5;

    @JsonDeserialize(contentUsing = LocalDateTimeDeserializer.class)
    private List<LocalDateTime> shiftList;

    public OneHourPeriod() {
        shiftList = new ArrayList<>();
    }

    public List<LocalDateTime> getShiftList() {
        return shiftList;
    }

    public void setShiftList(final List<LocalDateTime> shiftList) {
        this.shiftList = shiftList;
    }

    /**
     * Here is the logic of start a shift.
     * This method only is called if it is possible to sell. That means,
     * it is not possible call this rule before of the next shift.
     * @return datetime of when doggy will be available to work.
     */
    @Override
    public LocalDateTime startShift() {
        final LocalDateTime futureShift = LocalDateTime.now().plusMinutes(WAIT_MINUTES);
        shiftList.add(futureShift);

        if (shiftList.size() > MAX_TOTAL_TEST) {
            shiftList.remove(0);
            super.setNextShift(shiftList.get(0));
        } else if (shiftList.size() == MAX_TOTAL_TEST) {
            super.setNextShift(shiftList.get(0));
        } else {
            super.setNextShift(LocalDateTime.now());
        }
        return super.getNextShift();
    }

    /**
     * Only for testing
     * @return
     */
    public static int getWaitMinutes() {
        return WAIT_MINUTES;
    }

    /**
     * Only for testing
     * @return
     */
    public static int getMaxTotalTest() {
        return MAX_TOTAL_TEST;
    }
}
