package com.firehydrants.rules;

import com.firehydrants.db.entities.Constraint;
import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.ConstraintRepository;
import com.firehydrants.db.repositories.DoggyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class Orchestrator {

    @Autowired
    RulesFactory factory;
    @Autowired
    ConstraintRepository constraintRepository;
    @Autowired
    DoggyRepository doggyRepository;

    /**
     * This method call all the rules and find the date and time of the next shift.
     * Also it is here where we "unlock" our doggy changing its status, condition necessary
     * to take the next shift.
     * IMPROVEMENT:
     * This method could be async. E.g., each constraint can be a service and the orchestrator call of them.
     * An important consideration, if something is wrong, the Orchestrator should be in charge of returning the
     * unlock status to our doggy. A possible solution will be to set "the next shift" considering the worst case,
     * e.g. an hour.
     * @param doggy Doggy
     * @return doggy with the information of when will be available to work
     */
    public Doggy work(final Doggy doggy) {
        List<Constraint> constraints = constraintRepository.findAllByDoggy(doggy);

        // New employee
        if (constraints.isEmpty()) {
            constraints = constraintRepository.saveAll(factory.firstShift(doggy));
        }

        /**
         * If the logic for a rule is more complex, it will possible to call
         * the rules in parallel. Even, the rules could be another service.
         */
        constraints
                .forEach(
                    constraint -> {
                        final RuleBase rule = factory.constraintToRule(constraint);
                        rule.startShift();
                        constraint.setNextShift(rule.getNextShift());
                        constraint.setJsonData(rule.getJsonDump());
                    }
        );

        final Constraint nextShift = Collections.max(constraints);
        doggy.setNextShift(nextShift.getNextShift());

        constraints = constraintRepository.saveAll(constraints);
        doggy.setStatus(Doggy.Status.UnLock);
        doggy.setConstraints(constraints);
        return doggyRepository.saveAndFlush(doggy);
    }
}
