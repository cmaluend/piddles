package com.firehydrants.rules;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.time.LocalDateTime;

public abstract class RuleBase {

    @JsonIgnore
    private LocalDateTime nextShift;

    abstract LocalDateTime startShift();

    public LocalDateTime getNextShift() {
        return this.nextShift;
    }

    public void setNextShift(final LocalDateTime nextShift) {
        this.nextShift = nextShift;
    }

    /**
     * This method serialize the extra information from each rules.
     * It is store in the database as a Json.
     * @return Json string
     */
    @JsonIgnore
    public String getJsonDump() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        try {
            return objectMapper.writeValueAsString(this);
        } catch (final JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This method populates the object with the information from Json.
     * @param json Json string with additional values.
     */
    public void loadJson(final String json) {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        try {
            final RuleBase rule = objectMapper.readValue(json, this.getClass());
            BeanUtils.copyProperties(rule, this);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return String.format("%s: %s", this.getClass().getSimpleName(), getJsonDump());
    }
}
