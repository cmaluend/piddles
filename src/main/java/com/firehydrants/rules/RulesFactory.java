package com.firehydrants.rules;

import com.firehydrants.db.entities.Constraint;
import com.firehydrants.db.entities.Doggy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class RulesFactory {

    /**
     * This method creates a list of Constraints when the doggy has
     * its first shift.
     * @param doggy Doggy
     * @return constraints
     */
    public List<Constraint> firstShift(final Doggy doggy) {
        return Arrays.asList(
                new Constraint.Builder(doggy)
                        .setName(Constraint.Name.OneHourPeriod)
                        .build(),
                new Constraint.Builder(doggy)
                        .setName(Constraint.Name.FiveMinutesPeriod)
                        .build()
        );
    }

    /**
     * This method translate from constraint to Rule
     * @param constraint Constraint
     * @return Rule
     */
    public RuleBase constraintToRule(final Constraint constraint) {
        RuleBase rule = null;

        switch (constraint.getName()) {
            case FiveMinutesPeriod:
                rule = new FiveMinutesPeriod();
                break;
            case OneHourPeriod:
                rule = new OneHourPeriod();
                break;
            default:
                //As we are using enum, this will never happen.
                throw new RuntimeException("An existent constraint "+ constraint.getName());
        }
        rule.loadJson(constraint.getJsonData());
        rule.setNextShift(constraint.getNextShift());
        return rule;
    }

}
