package com.firehydrants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    /**
     * Configuration of H2
     * The database is in ram. So, it will be deleted and created every time.
     * @return dataSource
     */
    @Bean("MyDataSource")
    public DataSource dataSource() {
        final EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        final EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2)
                .addScript("db/db-prod.sql")
                .build();
        return db;
    }

    @Bean
    @Autowired
    public JdbcTemplate getJdbcTemplate(@Value("#{MyDataSource}") final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
