package com.firehydrants.models;

public final class CanSellHydrant {

    private boolean canSell;

    public boolean isCanSell() {
        return canSell;
    }

    public void canSell(final boolean canSell) {
        this.canSell = canSell;
    }

    private CanSellHydrant(final Builder builder) {
        canSell = builder.canSell;
    }

    public static class Builder {
        private boolean canSell;

        public Builder(){

        }

        public Builder canSell(final boolean canSell) {
            this.canSell = canSell;
            return this;
        }

        public CanSellHydrant build() {
            return new CanSellHydrant(this);
        }
    }
}
