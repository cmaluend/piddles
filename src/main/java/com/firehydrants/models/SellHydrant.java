package com.firehydrants.models;

public final class SellHydrant {

    private boolean sell;

    public boolean isSell() {
        return sell;
    }

    public void sell(final boolean sell) {
        this.sell = sell;
    }

    private SellHydrant(final Builder builder) {
        sell = builder.sell;
    }

    public static class Builder {
        private boolean sell;

        public Builder() {

        }

        public Builder sell(final boolean sell) {
            this.sell = sell;
            return this;
        }

        public SellHydrant build() {
            return new SellHydrant(this);
        }
    }
}
