package com.firehydrants;

import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.DoggyRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class FirehydrantsApplication {


    public static void main(final String[] args) {
        SpringApplication.run(FirehydrantsApplication.class, args);
    }

    /**
     * This method populate the Doggy table with some doggies
     * @param doggyRepository DoggyRepository
     * @return CommandLineRunner
     */
    @Bean
    CommandLineRunner init(final DoggyRepository doggyRepository) {
        return (evt) -> Arrays.asList(
//                "Puddles",
//                "Pyddles",
                "Piddles"
                )
                .forEach(
                    name -> {
                        doggyRepository.save(new Doggy(name));
                    }
                );
    }
}
