package com.firehydrants.rest.controllers;

import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.DoggyRepository;
import com.firehydrants.models.CanSellHydrant;
import com.firehydrants.models.SellHydrant;
import com.firehydrants.rules.Orchestrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@RestController
public class HydrantController {

    private final ReentrantLock lock = new ReentrantLock();

    @Autowired
    private DoggyRepository doggyRepository;

    @Autowired
    private Orchestrator orchestrator;

    /**
     * This method is for testing.
     * @return a list of doggies.
     */
    @RequestMapping(value = "/doggies", method = RequestMethod.GET)
    public List<Doggy> getAllDoggies() {
        return doggyRepository.findAll();
    }

    /**
     * This method is for testing.
     * @return the first doggy UnLock.
     */
    @RequestMapping(value = "/first", method = RequestMethod.GET)
    public Doggy getFirstDoggy() {
        return doggyRepository.findFirstByStatusAndNextShiftIsLessThan(Doggy.Status.UnLock, LocalDateTime.now());
    }

    /**
     * This method check if there is a worker (doggie) UnLock.
     * @return true or false
     */
    @RequestMapping(value = "/canSellHydrant", method = RequestMethod.GET)
    public CanSellHydrant canSellHydrant() {
        return new CanSellHydrant.Builder().canSell(
                doggyRepository.countByStatusAndNextShiftIsLessThan(Doggy.Status.UnLock, LocalDateTime.now()) > 0
        ).build();
    }

    /**
     * This method return true if is possible to sell an hydrant.
     * Some considerations:
     * - Should be thread safe
     * - We only need a worker to start a selling.
     * @return true if we can sell, and false in other case.
     */
    @RequestMapping(value = "/sellHydrant", method = RequestMethod.GET)
    public SellHydrant sellHydrant() {
        final SellHydrant sellHydrant = new SellHydrant.Builder().sell(false).build();

        /**
         * This is the critical block of code because we don't want data racing.
         * We lock the minimum for avoid any concurrency problem.
         */
        lock.lock();
        Doggy doggy = null;
        try {
            doggy = doggyRepository.findFirstByStatusAndNextShiftIsLessThan(Doggy.Status.UnLock, LocalDateTime.now());
            if (doggy != null) {
                doggy.setStatus(Doggy.Status.Lock);
                doggy = doggyRepository.saveAndFlush(doggy);
                sellHydrant.sell(true);
            } else {
                return sellHydrant;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            return sellHydrant;
        } finally {
            lock.unlock();
        }
        /**
         * Call the constrains for a doggy.
         * This could be async as I already have a employee to test.
         * The important point is this task should be finish before
         * of 5 minutes (Possible time to take another task.)
         */
        orchestrator.work(doggy);
        return sellHydrant;
    }
}
