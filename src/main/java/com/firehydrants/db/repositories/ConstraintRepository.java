package com.firehydrants.db.repositories;

import com.firehydrants.db.entities.Constraint;
import com.firehydrants.db.entities.Doggy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConstraintRepository extends JpaRepository<Constraint, Long> {

    List<Constraint> findAllByDoggy(final Doggy doggy);
}
