package com.firehydrants.db.repositories;

import com.firehydrants.db.entities.Doggy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface DoggyRepository extends JpaRepository<Doggy, Long> {

    Doggy findFirstByStatusAndNextShiftIsLessThan(final Doggy.Status status, final LocalDateTime after);

    Integer countByStatusAndNextShiftIsLessThan(final Doggy.Status status, final LocalDateTime after);
}
