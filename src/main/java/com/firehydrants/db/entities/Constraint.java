package com.firehydrants.db.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public final class Constraint implements Comparable<Constraint> {

    public enum Name {
        FiveMinutesPeriod, OneHourPeriod
    }

    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Doggy doggy;
    private Name name;
    private LocalDateTime nextShift;
    @Column(columnDefinition = "clob")
    private String jsonData;

    public Constraint() {}

    private Constraint(final Builder builder) {
        doggy = builder.doggy;
        name = builder.name;
        nextShift = builder.nextShift;
        jsonData = builder.jsonData;
    }

    public Long getId() {
        return id;
    }

    public Doggy getDoggy() {
        return doggy;
    }

    public LocalDateTime getNextShift() {
        return nextShift;
    }

    public String getJsonData() {
        return jsonData;
    }

    public Name getName() {
        return name;
    }

    public void setName(final Name name) {
        this.name = name;
    }

    public void setNextShift(final LocalDateTime nextShift) {
        this.nextShift = nextShift;
    }

    public void setJsonData(final String jsonData) {
        this.jsonData = jsonData;
    }

    @Override
    public String toString() {
        return String.format("%s[%s] %s", name, nextShift, jsonData);
    }

    @Override
    public int compareTo(final Constraint constraint) {
        return this.getNextShift().compareTo(constraint.getNextShift());
    }

    public static class Builder {

        private Doggy doggy;
        private Name name;
        private LocalDateTime nextShift;
        private String jsonData;

        public Builder(final Doggy doggy) {
            this.doggy = doggy;
            this.nextShift = LocalDateTime.now();
            this.jsonData = "{}";
        }

        public Constraint build() {
            return new Constraint(this);
        }

        public Builder setName(final Name name) {
            this.name = name;
            return this;
        }

        public Builder setNextShift(final LocalDateTime nextShift) {
            this.nextShift = nextShift;
            return this;
        }

        public Builder setJsonData(final String jsonData) {
            this.jsonData = jsonData;
            return this;
        }

    }
}
