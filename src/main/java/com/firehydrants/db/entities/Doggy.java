package com.firehydrants.db.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Doggy {

    public enum Status {
        UnLock, Lock
    }

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private LocalDateTime nextShift;
    private Status status;

    @OneToMany(mappedBy = "doggy")
    private List<Constraint> constraints = new ArrayList<>();


    private Doggy() {
    }

    public Doggy(final String name) {
        this.name = name;
        //Sorry doggy, there is not onboarding plan!
        this.nextShift = LocalDateTime.now();
        this.status = Status.UnLock;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getNextShift() {
        return nextShift;
    }

    public void setNextShift(final LocalDateTime nextShift) {
        this.nextShift = nextShift;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<Constraint> constraints) {
        this.constraints = constraints;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)[%s]", name, status, nextShift);
    }
}
