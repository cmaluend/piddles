package com.firehydrants.rest.controllers;

import com.firehydrants.db.entities.Doggy;
import com.firehydrants.rules.FiveMinutesPeriod;
import com.firehydrants.rules.OneHourPeriod;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;

/**
 * Piddles with 5 minutes constraint and 5 test in a hour.
 */
public class SecondPartTest extends IntegrationBase {

    @Before
    public void setup() {
        constraintRepository.deleteAllInBatch();
        doggyRepository.deleteAllInBatch();
        doggyRepository.save(new Doggy("Piddles"));
    }

    @Test
    public void singleUserCase() {
        Assert.assertTrue(controller.canSellHydrant().isCanSell());
        Assert.assertTrue(controller.sellHydrant().isSell());
        Assert.assertFalse(controller.canSellHydrant().isCanSell());
        Assert.assertFalse(controller.sellHydrant().isSell());
    }

    @Test
    public void multipleUsersCase() throws ExecutionException, InterruptedException {
        concurrentCallsValidation();
    }

    @Test
    public void multipleUsersCaseWithRules() throws ExecutionException, InterruptedException {
        LocalDateTime oneHour = LocalDateTime.now().plusMinutes(OneHourPeriod.getWaitMinutes()+1);
        for(int i=0; i< OneHourPeriod.getMaxTotalTest(); i++) {
            concurrentCallsValidation();
            skipTime(FiveMinutesPeriod.getWaitMinutes());
        }
        long totalCanSell = multiCanSellHydrant(getControllers()).stream()
                .filter(canSellHydrant -> canSellHydrant.isCanSell())
                .count();

        //No one can sell now until 1 hour from the first transaction.
        Assert.assertEquals(0, totalCanSell);

        //Wait oneHour - localDateTime.now() -> I already wait in the for 5 minutes x OneHourPeriod.getMaxTotalTest()
        skipTime(Duration.between(LocalDateTime.now(), oneHour).toMinutes());
        concurrentCallsValidation();
        //The next will be in another hour
        skipTime(OneHourPeriod.getWaitMinutes());
        concurrentCallsValidation();
    }


}
