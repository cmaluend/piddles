package com.firehydrants.rest.controllers;

import com.firehydrants.db.entities.Constraint;
import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.ConstraintRepository;
import com.firehydrants.db.repositories.DoggyRepository;
import com.firehydrants.models.CanSellHydrant;
import com.firehydrants.models.SellHydrant;
import com.firehydrants.rules.OneHourPeriod;
import com.firehydrants.rules.RulesFactory;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class IntegrationBase {

    @Autowired
    protected HydrantController controller;
    @Autowired
    protected DoggyRepository doggyRepository;
    @Autowired
    protected ConstraintRepository constraintRepository;
    @Autowired
    protected RulesFactory factory;

    /**
     * This function return a list of controller for testing.
     * The min value is 3, because there is a test with 3 doggies.
     * @return list of controllers
     */
    protected HydrantController[] getControllers() {
        return Collections.nCopies(10, controller).toArray(new HydrantController[]{});
    }

    protected List<CanSellHydrant> multiCanSellHydrant(HydrantController... controllers) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(controllers.length);

        List<CompletableFuture<CanSellHydrant>> relevanceFutures = Arrays.asList(controllers).stream()
                .map(controller -> CompletableFuture.supplyAsync(() -> controller.canSellHydrant(), executor))
                .collect(Collectors.toList());

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                relevanceFutures.toArray(new CompletableFuture[relevanceFutures.size()])
        );

        CompletableFuture<List<CanSellHydrant>> allContentsFuture = allFutures.thenApply(v -> {
            return relevanceFutures.stream()
                    .map(canSellHydrantCompletableFuture -> canSellHydrantCompletableFuture.join())
                    .collect(Collectors.toList());
        });

        return allContentsFuture.get();
    }

    protected List<SellHydrant> multiSellHydrant(HydrantController... controllers) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(controllers.length);

        List<CompletableFuture<SellHydrant>> relevanceFutures = Arrays.asList(controllers).stream()
                .map(controller -> CompletableFuture.supplyAsync(() -> controller.sellHydrant(), executor))
                .collect(Collectors.toList());

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                relevanceFutures.toArray(new CompletableFuture[relevanceFutures.size()])
        );

        CompletableFuture<List<SellHydrant>> allContentsFuture = allFutures.thenApply(v -> {
            return relevanceFutures.stream()
                    .map(sellHydrantCompletableFuture -> sellHydrantCompletableFuture.join())
                    .collect(Collectors.toList());
        });

        return allContentsFuture.get();
    }

    protected void concurrentCallsValidation() throws ExecutionException, InterruptedException {
        HydrantController[] controllers = getControllers();
        long totalCanSell = multiCanSellHydrant(controllers).stream()
                .filter(canSellHydrant -> canSellHydrant.isCanSell())
                .count();

        //all of them can sell
        Assert.assertEquals(controllers.length, totalCanSell);
        List<SellHydrant> sellHydrantList = multiSellHydrant(controllers);
        long totalSell = sellHydrantList.stream()
                .filter(sellHydrant -> sellHydrant.isSell())
                .count();
        //only one can sell
        Assert.assertEquals(1, totalSell);

        totalCanSell = multiCanSellHydrant(controllers).stream()
                .filter(canSellHydrant -> canSellHydrant.isCanSell())
                .count();

        //No one can sell now.
        Assert.assertEquals(0, totalCanSell);
    }

    /**
     * This function update the times, instead of sleep some minutes.
     * ain't nobody got time for that!
     */
    protected void skipTime(final long minutes) {
        List<Doggy> doggies = doggyRepository.findAll();
        for (Doggy doggy:doggies) {
            LocalDateTime localDateTime = doggy.getNextShift();
            doggy.setNextShift(localDateTime.minusMinutes(minutes));
            doggy = doggyRepository.saveAndFlush(doggy);
            updateConstraints(doggy, minutes);
        }
    }

    private void updateConstraints(final Doggy doggy, final long minutes) {
        List<Constraint> constraints = constraintRepository.findAllByDoggy(doggy);
        for (Constraint constraint: constraints) {
            LocalDateTime localDateTime = constraint.getNextShift();
            constraint.setNextShift(localDateTime.minusMinutes(minutes));
            // Update OneHourPeriod List<LocalDataTime>
            if (constraint.getName()==Constraint.Name.OneHourPeriod) {
                OneHourPeriod oneHourPeriod = (OneHourPeriod)factory.constraintToRule(constraint);

                List<LocalDateTime> shiftList = oneHourPeriod.getShiftList().stream()
                                    .map(shift -> shift.minusMinutes(minutes))
                                    .collect(Collectors.toList());
                oneHourPeriod.setShiftList(shiftList);
                constraint.setJsonData(oneHourPeriod.getJsonDump());
            }
            constraintRepository.save(constraint);
        }
    }
}
