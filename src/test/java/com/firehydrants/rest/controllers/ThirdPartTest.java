package com.firehydrants.rest.controllers;

import com.firehydrants.db.entities.Doggy;
import com.firehydrants.models.SellHydrant;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Piddles, Puddles, and Pyddles with 5 minutes constraint and 5 test in an hour.
 */
public class ThirdPartTest extends IntegrationBase {

    @Before
    public void setup() {
        constraintRepository.deleteAllInBatch();
        doggyRepository.deleteAllInBatch();
        doggyRepository.save(new Doggy("Piddles"));
        doggyRepository.save(new Doggy("Pyddles"));
        doggyRepository.save(new Doggy("Puddles"));
    }

    @Test
    public void singleUserCase() {
        Assert.assertTrue(controller.canSellHydrant().isCanSell());
        Assert.assertTrue(controller.sellHydrant().isSell());
        Assert.assertTrue(controller.sellHydrant().isSell());
        Assert.assertTrue(controller.sellHydrant().isSell());
        Assert.assertFalse(controller.canSellHydrant().isCanSell());
        Assert.assertFalse(controller.sellHydrant().isSell());
    }

    @Test
    public void multipleUsersCase() throws ExecutionException, InterruptedException {
        HydrantController[] controllers = getControllers();
        long totalCanSell = multiCanSellHydrant(controllers).stream()
                .filter(canSellHydrant -> canSellHydrant.isCanSell())
                .count();

        //all of them can sell
        Assert.assertEquals(controllers.length, totalCanSell);
        List<SellHydrant> sellHydrantList = multiSellHydrant(controllers);
        long totalSell = sellHydrantList.stream()
                .filter(sellHydrant -> sellHydrant.isSell())
                .count();
        //only one can sell
        Assert.assertEquals(3, totalSell);

        totalCanSell = multiCanSellHydrant(controllers).stream()
                .filter(canSellHydrant -> canSellHydrant.isCanSell())
                .count();

        //No one can sell now.
        Assert.assertEquals(0, totalCanSell);
    }

}
