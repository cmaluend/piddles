package com.firehydrants.rest.controllers;

import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.DoggyRepository;
import com.firehydrants.rules.Orchestrator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HydrantControllerTest {

    @InjectMocks
    private HydrantController controller;

    @Mock
    private DoggyRepository doggyRepository;
    @Mock
    private Orchestrator orchestrator;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCallSellHydrantAndThereIsADogThenSuccess() {
        final Doggy doggy = new Doggy("Brian Griffin");
        Mockito.when(doggyRepository.findFirstByStatusAndNextShiftIsLessThan(Mockito.any(), Mockito.any())).thenReturn(doggy);

        Assert.assertEquals(Doggy.Status.UnLock, doggy.getStatus());
        Assert.assertTrue("Should be possible to sell", controller.sellHydrant().isSell());
        Assert.assertEquals(Doggy.Status.Lock, doggy.getStatus());
    }

    @Test
    public void whenCallSellHydrantAndThereIsntAnyDogThenFalse() {
        Mockito.when(doggyRepository.findFirstByStatusAndNextShiftIsLessThan(Mockito.any(), Mockito.any())).thenReturn(null);
        Assert.assertFalse("Should be possible to sell", controller.sellHydrant().isSell());
    }

    @Test
    public void whenCallSellHydrantAndThereIsAnOrchestrationExceptionThenFalse() {
        Mockito.when(orchestrator.work(Mockito.any())).thenThrow(new RuntimeException("Ups!"));
        Assert.assertFalse("Shouldn't be possible to sell", controller.sellHydrant().isSell());
    }
}
