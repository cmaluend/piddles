package com.firehydrants.rest.controllers;

import com.firehydrants.db.entities.Constraint;
import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.ConstraintRepository;
import com.firehydrants.db.repositories.DoggyRepository;
import com.firehydrants.rules.FiveMinutesPeriod;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutionException;

/**
 * Piddles and only 5 minutes constraint
 */
public class FirstPartTest extends IntegrationBase {

    @Autowired
    private HydrantController controller;
    @Autowired
    private DoggyRepository doggyRepository;
    @Autowired
    private ConstraintRepository constraintRepository;

    @Before
    public void setup() {
        constraintRepository.deleteAllInBatch();
        doggyRepository.deleteAllInBatch();
        final Doggy doggy = new Doggy("Oddie");
        final Constraint constraint = new Constraint.Builder(doggy)
                .setName(Constraint.Name.FiveMinutesPeriod)
                .setJsonData("{}")
                .build();
        doggyRepository.save(doggy);
        constraintRepository.save(constraint);

    }

    @Test
    public void singleUserCase() {
        Assert.assertTrue(controller.canSellHydrant().isCanSell());
        Assert.assertTrue(controller.sellHydrant().isSell());
        Assert.assertFalse(controller.canSellHydrant().isCanSell());
        Assert.assertFalse(controller.sellHydrant().isSell());
    }

    @Test
    public void multipleUsersCase() throws ExecutionException, InterruptedException {
        concurrentCallsValidation();
    }

    @Test
    public void multipleUsersCaseWithRules() throws ExecutionException, InterruptedException {
        concurrentCallsValidation();
        skipTime(FiveMinutesPeriod.getWaitMinutes());
        concurrentCallsValidation();
    }
}
