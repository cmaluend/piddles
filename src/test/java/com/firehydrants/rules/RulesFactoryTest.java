package com.firehydrants.rules;

import com.firehydrants.db.entities.Constraint;
import com.firehydrants.db.entities.Doggy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RulesFactoryTest {

    @Autowired
    private RulesFactory rulesFactory;
    @Mock
    private Doggy doggy;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCallConstraintToRuleThenRetunAValidRule() {

        final Constraint constraint = new Constraint.Builder(doggy)
                .setName(Constraint.Name.OneHourPeriod)
                .setNextShift(LocalDateTime.now())
                .setJsonData("{\"shiftList\":[\"2018-03-25T12:00:02.845\"]}")
                .build();

        final RuleBase rule = rulesFactory.constraintToRule(constraint);
        Assert.assertEquals(constraint.getName().toString(), rule.getClass().getSimpleName());
        Assert.assertEquals(constraint.getNextShift(), rule.getNextShift());
        /**
         * The JSon is converted to a List<LocalDateTime> in the rulesFactory, and converter
         * again to a String when rule.getJsonDump
         */
        Assert.assertEquals(constraint.getJsonData(), rule.getJsonDump());

    }
}
