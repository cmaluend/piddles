package com.firehydrants.rules;

import com.firehydrants.db.entities.Doggy;
import com.firehydrants.db.repositories.ConstraintRepository;
import com.firehydrants.db.repositories.DoggyRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrchestratorTest {

    @Autowired
    @InjectMocks
    private Orchestrator orchestrator;

    @Mock
    ConstraintRepository constraintRepository;
    @Mock
    DoggyRepository doggyRepository;

    @Autowired
    private RulesFactory rulesFactory;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenNewEmployeeCallWorkThenGetTheNextShift() {
        Doggy doggy = new Doggy("Snoop Dogg");
        Mockito.when(constraintRepository.saveAll(Mockito.anyList())).thenReturn(rulesFactory.firstShift(doggy));
        Mockito.when(doggyRepository.saveAndFlush(doggy)).thenReturn(doggy);
        doggy= orchestrator.work(doggy);
        Assert.assertNotNull(doggy.getNextShift());
        Assert.assertTrue("The next shift should be, in the best case, after 5 minutes",Duration.between(LocalDateTime.now(), doggy.getNextShift()).toMinutes() > 3);
    }

    @Test
    public void whenCallWorkThenGetTheNextShift() {
        Doggy doggy = new Doggy("Snoop Dogg");
        Mockito.when(constraintRepository.findAllByDoggy(Mockito.any())).thenReturn(rulesFactory.firstShift(doggy));
        Mockito.when(doggyRepository.saveAndFlush(doggy)).thenReturn(doggy);
        doggy= orchestrator.work(doggy);
        Assert.assertNotNull(doggy.getNextShift());
        Assert.assertTrue("The next shift should be, in the best case, after 5 minutes",Duration.between(LocalDateTime.now(), doggy.getNextShift()).toMinutes() > 3);
    }
}
