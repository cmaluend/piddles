package com.firehydrants.rules;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FiveMinutesPeriodTest {

    @Test
    public void whenStartShiftThenTheNextShitIsIn5Minutes() {
        final LocalDateTime nextShift = LocalDateTime.now().plusMinutes(FiveMinutesPeriod.getWaitMinutes());
        final LocalDateTime ruleTime = new FiveMinutesPeriod().startShift();
        Assert.assertTrue("The difference should be less than 500 miliseconds", Duration.between(nextShift, ruleTime).toMillis() < 500);
    }
}
