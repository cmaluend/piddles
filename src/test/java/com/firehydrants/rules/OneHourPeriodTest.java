package com.firehydrants.rules;

import org.apache.tomcat.jni.Local;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OneHourPeriodTest {

    @Test
    public void whenStartShiftThenTheNextShitIsIn5Minutes() throws InterruptedException {

        final OneHourPeriod oneHourPeriod = new OneHourPeriod();
        LocalDateTime nextShift = LocalDateTime.now();
        LocalDateTime oneHourAfter = nextShift.plusHours(1);

        //The first n-1 should return "now()" because there is room for another shift
        for (int i = 0; i < OneHourPeriod.getMaxTotalTest()-1; i++) {
            nextShift = LocalDateTime.now();
            Assert.assertTrue("The difference should be less than 100 miliseconds", Duration.between(nextShift, oneHourPeriod.startShift()).toMillis() < 100);
            Thread.sleep(1000);
        }
        //After completes the quote, the next shift should be in an hour from the
        Assert.assertTrue("The difference should be less than 100 miliseconds", Duration.between(oneHourAfter, oneHourPeriod.startShift()).toMillis() < 100);
    }
}
